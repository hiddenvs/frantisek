#ifndef LOG_H
#define LOG_H

#include <string>
#include <vector>
#include <ostream>

using namespace std;

class QSO;

class Log
{
  public:
    Log(string filename);
    ~Log();

    vector<QSO*> data;

    void write(string filename);

  private:
    void make_page(ostream& os, int& i);
};

#endif // LOG_H
